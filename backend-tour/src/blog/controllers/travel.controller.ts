import { Controller ,Post ,Res ,Body,HttpStatus } from '@nestjs/common';
import { TravelService } from '../services/travel.service';
import { TravelDto } from '../dto/travel.dto';
@Controller('travel')
export class TravelController {
    constructor(private TravelService: TravelService) { }

    @Post('/post')
    async addPost(@Res() res, @Body() TravelDto: TravelDto) {
        const newPost = await this.TravelService.createTravel(TravelDto);
        return res.status(HttpStatus.OK).json({
            message: "Post has been submitted successfully!",
            post: newPost
        })
    }
}

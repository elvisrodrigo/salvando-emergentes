import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Mongoose } from 'mongoose';
import { TravelSchema } from '../schemas/travel.schema';
import { TravelController } from '../controllers/travel.controller';
import { TravelService } from '../services/travel.service';
@Module({
    imports:[ 
        MongooseModule.forFeature([{ name: 'Travel',schema: TravelSchema }])
    ],
    controllers: [TravelController],
    providers:[TravelService]
})
export class TravelesModule {}

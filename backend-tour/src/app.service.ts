import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  GetHello(): string {
    return 'Welcome Get';
  }
  PostHello(): string {
    return 'Welcome Post';
  }
  PutHello(): string {
    return 'Welcome Put';
  }
  DeleteHello(): string {
    return 'Welcome Delete';
  }

}

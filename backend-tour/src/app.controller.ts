import { Controller, Get ,Post ,Put ,Delete } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  GetHello(): string {
    return this.appService.GetHello();
  }
  @Post()
  PostHello(): string {
    return this.appService.PostHello();
  }
  @Put()
  PutHello(): string {
    return this.appService.PutHello();
  }
  @Delete()
  DeleteHello(): string {
    return this.appService.DeleteHello();
  }
}
